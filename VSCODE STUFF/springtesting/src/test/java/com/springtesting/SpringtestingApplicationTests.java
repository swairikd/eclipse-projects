package com.springtesting;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringtestingApplicationTests {

	private Calculator calc = new Calculator();

	@Test
	void contextLoads() {
	}

	@Test
	void testSum() {
		int expectedResult = 60;
		int actualResult = calc.sum(10, 20, 30);

		assertThat(actualResult).isEqualTo(expectedResult);
	}

	@Test
	void testMul() {
		int expectedResult = 6000;
		int actualResult = calc.mul(10, 20, 30);

		assertThat(actualResult).isEqualTo(expectedResult);
	}

}
