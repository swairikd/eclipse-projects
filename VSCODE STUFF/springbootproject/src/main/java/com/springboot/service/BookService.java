package com.springboot.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.model.Book;
import com.springboot.repository.BookRepository;

@Service
public class BookService {
    private static List<Book> list = new ArrayList<>();

    @Autowired
    private BookRepository bookRepository;

    static {
        list.add(new Book(1001, "Spring Tutorial", "XYZ"));
        list.add(new Book(1002, "Spring Boot Tutorial", "XYZA"));
        list.add(new Book(1003, "Java Tutorial", "XYZB"));
        list.add(new Book(1004, "Spring MVC Tutorial", "XYZC"));
    }

    public List<Book> getAllBooks() {
        // return list;
        List<Book> books = (List<Book>) this.bookRepository.findAll();
        return books;
    }

    public Book getBookById(int id) {
        Book book = null;

        // book = list
        // .stream()
        // .filter(b -> b.getId() == id)
        // .findFirst()
        // .get();

        try {
            book = (Book) this.bookRepository.findById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return book;
    }

    public Book addBook(Book book) {
        // BookService.list.add(book);
        bookRepository.save(book);
        return book;
    }

    public Book deleteBook(int id) {
        Book book = this.getBookById(id);

        bookRepository.deleteById(id);

        return book;
    }

    public Book updateBook(Book book) {
        // for (Book b : list) {
        // if (b.getId() == book.getId()) {
        // b.setTitle(book.getTitle());
        // b.setAuthor(book.getAuthor());
        // // return true;
        // }
        // }
        deleteBook(book.getId());
        bookRepository.save(book);
        return book;
    }

}
