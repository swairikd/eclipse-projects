package com.springcore.lifecycle;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class SecondItem implements InitializingBean, DisposableBean {
	private double price;

	public SecondItem() {
		super();
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		System.out.println("Setting second item price");
		this.price = price;
	}

	@Override
	public String toString() {
		return "SecondItem [price=" + price + "]";
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("Welcome, initialisation done");
	}

	public void destroy() throws Exception {
		System.out.println("Work done, destroying bean");
	}

}
