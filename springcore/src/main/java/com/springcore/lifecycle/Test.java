package com.springcore.lifecycle;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext(
				"./com/springcore/lifecycle/lifecycleConfig.xml");

//		Item item1 = (Item) context.getBean("firstItem");
//		System.out.println(item1);
//
//		SecondItem item2 = (SecondItem) context.getBean("secondItem");
//		System.out.println(item2);

		Example ex1 = (Example) context.getBean("example");
		System.out.println(ex1);

		context.registerShutdownHook();

	}

}
