package com.springcore.autowire;

import org.springframework.beans.factory.annotation.Autowired;

public class Emp {
	@Autowired
	private Address address;

	public Emp() {
		super();
		System.out.println("inside base constructor");
	}

//	@Autowired
	public Emp(Address address) {
		super();
		System.out.println("Inside constructor");
		this.address = address;
	}

	public Address getAddress() {
		return address;
	}

//	@Autowired
	public void setAddress(Address address) {
		System.out.println("Setting value");
		this.address = address;
	}

	@Override
	public String toString() {
		return "Emp [address=" + address + "]";
	}

}
