package com.springsecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloWorldController {

	@GetMapping("/helloworld")
	public String helloWorld() {
		return "HelloWorld";
	}
	
	@GetMapping("/test")
	public String hello() {
		return "test";
	}
	
	@GetMapping("/testHi")
	public String hi() {
		return "hi from test";
	}
	
	
}
