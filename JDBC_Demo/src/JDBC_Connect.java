import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBC_Connect {

	public static void main(String[] args) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/mydb?characterEncoding=latin1&useConfigs=maxPerformance", "root",
					"root");
			Statement smt = con.createStatement();
//			smt.executeUpdate(
//						"create table emp(eno number, ename varchar(20), esal number)"
//							);
//			System.out.println("Table created successfully");
			ResultSet res = smt.executeQuery("select * from emp");
			while(res.next())
				System.out.println(res.getString("EMPID"));
//						+ " : " + res.getString("firstname"));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
