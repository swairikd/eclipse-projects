package com.springboot.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {

//	@ResponseBody
	@GetMapping("/test")
	public String test() {
		return "home";
	}

}
