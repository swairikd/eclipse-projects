package com.springboot.controller;

import java.awt.print.Book;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookController {

	@GetMapping("/books")
	public Book getBooks() {
		Book book = new Book();
		book.setId(12500);
		book.setTitle("Java Spring Tutorial");
		book.setAuthor("ABC");
		return book;
	}

}
