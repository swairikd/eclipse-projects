package com.json;

import com.google.gson.Gson;

public class App {

	public static void main(String[] args) {
		Student student = new Student(1, "Bruce", "Gotham", "911");

		Gson gson = new Gson();

		String str = gson.toJson(student);
		System.out.println(str);

//		System.out.println(new Gson().toJson(student));

	}

}
