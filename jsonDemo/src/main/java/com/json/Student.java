package com.json;

public class Student {
	private int sid;
	private String sname;
	private String city;
	private String contact;

	public Student() {
		super();
	}

	public Student(int sid, String sname, String city, String contact) {
		super();
		this.sid = sid;
		this.sname = sname;
		this.city = city;
		this.contact = contact;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	@Override
	public String toString() {
		return "Student [sid=" + sid + ", sname=" + sname + ", city=" + city + ", contact=" + contact + "]";
	}
	
}
