package com.spring.jdbcDemo;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.jdbcDemo.dao.StudentDaoImpl;
import com.spring.jdbcDemo.model.Student;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
		System.out.println("Starting...");
		ApplicationContext context = new ClassPathXmlApplicationContext("./com/spring/jdbcDemo/config.xml");

		StudentDaoImpl studentDao = context.getBean("studentDao", StudentDaoImpl.class);

		Student student = new Student();
		
		/*
//		INSERT
		student.setId(101);
		student.setName("Peter");
		student.setCity("Queens");

		int result = studentDao.insert(student);

		System.out.println(result + " row(s) inserted");
		*/

		/*
//		UPDATE
		student.setId(101);
		student.setName("Bruce");
		student.setCity("Gotham");

		int result = studentDao.modify(student);

		System.out.println(result + " row(s) updated");
		 */
		
		/*
//		DELETE
		int result = studentDao.delete(101);
		
		System.out.println(result + " row(s) deleted");
		*/
		
		/*
//		SELECT 1 row
		Student student2 = studentDao.getStudent(101);
		System.out.println(student2);
		*/

		List<Student> students = studentDao.getAllStudents();
		for(Student s : students) {
			System.out.println(s);
		}
		
	}
}
