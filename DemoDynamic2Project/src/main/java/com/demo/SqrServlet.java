package com.demo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SqrServlet extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
//		int r = Integer.parseInt(req.getParameter("result"));
		int r = (int) req.getAttribute("sum")
		
		PrintWriter out = res.getWriter();
		out.println("Square of " + r + " is " + r * r + " from sqrPost");
		
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
//		redirection does only get requests...so after redirection from add it hits here
//		int r = Integer.parseInt(req.getParameter("result"));
		
//		HttpSession session = req.getSession();
//		int r = (int) session.getAttribute("result");
		
		int r = 0;
		Cookie cookies[] = req.getCookies();
		for(Cookie c : cookies) {
			if(c.getName().equals("result")) {
				r = Integer.parseInt(c.getValue());
			}
		}
		
		PrintWriter out = res.getWriter();
		out.println("Square of " + r + " is " + r * r + " from sqrGet");
				
	}
}
