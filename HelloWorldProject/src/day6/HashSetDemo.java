package day6;

import java.util.HashSet;
import java.util.Set;

public class HashSetDemo {

	public static void main(String[] args) {
		Set<Employee> hashSet = new HashSet<Employee>();
		
		Employee emp1 = new Employee(101, "Bruce", 25000);
		Employee emp2 = new Employee(102, "Peter", 25000);
		Employee emp3 = new Employee(103, "Tony", 25000);
		Employee emp4 = new Employee(104, "Clark", 25000);

		hashSet.add(emp1);
		hashSet.add(emp2);
		hashSet.add(emp3);
		hashSet.add(emp4);
		hashSet.add(emp4);
		
		for(Employee emp : hashSet) {
			System.out.println(emp);
		}
		
	}

}
