package day6;

import java.util.*;

public class CollectionDemo {

	public static void main(String[] args) {
		List list = new ArrayList();
		list.add("Java");
		list.add("Cpp");
		list.add("Python");
		list.add("Kotlin");
		
		//for each
		for(Object input : list) {
			System.out.println(input);
		}
		
//		Iterator
		Iterator itr = list.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		
//		List Iterator
		ListIterator itrl = list.listIterator();
		while(itrl.hasNext()) {
			System.out.println(itrl.next());
		}
		
//		enumeration - used when creating vectors
		Vector vector = new Vector();
		vector.add("Java");
		vector.add("Cpp");
		vector.add("Python");
		vector.add("Kotlin");
		
		Enumeration enumv = vector.elements();
		while(enumv.hasMoreElements()) {
			System.out.println(enumv.nextElement());
		}
		
	}

}
