package day6;

public class TesterClient {

	public static void main(String[] args) {
		Account account = new Account();
		AccountHolder accHolder = new AccountHolder(account);
		
		Thread t1 = new Thread(accHolder);
		Thread t2 = new Thread(accHolder);
		
		t1.setName("Bruce");
		t2.setName("Peter");
		
		t1.start();
		t2.start();
		
	}

}
