package day6;

public class GenericDemo<T> {
	T container;
	
	public GenericDemo() {
	}

	public GenericDemo(T container) {
		this.container = container;
	}
	
	public Object getValue() {
		return this.container;
	}
	
}
