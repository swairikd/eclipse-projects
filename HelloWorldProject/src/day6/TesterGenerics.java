package day6;

public class TesterGenerics {

	public static void main(String[] args) {
		GenericDemo<String> obj1 = new GenericDemo<String>("Learning Generics");
//		GenericDemo obj2 = new GenericDemo(2344);
		
		System.out.println(obj1.getValue());
		System.out.println(obj1.container.getClass().getName());
//		System.out.println(obj2.getValue());
		
		GenericDemo<Integer> obj2 = new GenericDemo<Integer>(2345);
		
		System.out.println(obj2.getValue());
		System.out.println(obj2.container.getClass().getName());
		
	}

}
