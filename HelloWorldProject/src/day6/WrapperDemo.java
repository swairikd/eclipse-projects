package day6;

public class WrapperDemo {

	public static void main(String[] args) {
		int i = 5;	// primitive type
		System.out.println(i);
		
		Integer i2 = new Integer(5);
//		<Integer>
		System.out.println(i2);
		
//		boxing unboxing
		
		Integer i3 = new Integer(i); // primitive value inside object => boxing -> wrapping
		System.out.println(i3);
		
		int j = i2.intValue();	// taking object value and putting in primitve -> unboxing -> unwrapping
		System.out.println(j);
		
		Integer i4 = i;	// java will do this automatically, autoboxing
		System.out.println(i4);
		
		int k = i2; 	// autounboxing
		System.out.println(k);
		
//		Primitive works faster
		
	}

}
