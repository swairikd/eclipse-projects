package day6;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		Employee emp1 = new Employee(101, "Bruce", 25000);
		Employee emp2 = new Employee(102, "Peter", 25000);
		Employee emp3 = new Employee(103, "Tony", 25000);
		Employee emp4 = new Employee(104, "Clark", 25000);
		
		Set<Employee> empSet = new TreeSet<Employee>().descendingSet();
		
		empSet.add(emp1);
		empSet.add(emp2);
		empSet.add(emp3);
		empSet.add(emp4);
		
		for(Employee emp : empSet) {
			System.out.println(emp);
		}
	}

}
