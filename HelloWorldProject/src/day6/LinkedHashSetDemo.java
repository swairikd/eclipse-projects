package day6;

import java.util.LinkedHashSet;
import java.util.Set;

public class LinkedHashSetDemo {
	public static void main(String[] args) {
		Employee emp1 = new Employee(101, "Bruce", 25000);
		Employee emp2 = new Employee(102, "Peter", 25000);
		Employee emp3 = new Employee(103, "Tony", 25000);
		Employee emp4 = new Employee(104, "Clark", 25000);
		
		Set<Employee> linkedHashSet = new LinkedHashSet<>();
		linkedHashSet.add(emp1);
		linkedHashSet.add(emp2);
		linkedHashSet.add(emp3);
		linkedHashSet.add(emp4);
		
		for(Employee emp : linkedHashSet) {
			System.out.println(emp);
		}
		
	}
}
