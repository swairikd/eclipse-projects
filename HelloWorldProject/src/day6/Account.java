package day6;

public class Account {
	private int balance = 10000;
	public int getBalance() {
		return this.balance;
	}
	
	public void withdraw(int amount) {
		balance -= amount;
	}
	
}
