package day6;

public class AccountHolder implements Runnable {
	private Account account;
	
	public AccountHolder(Account account) {
		this.account = account;
	}

	@Override
	public void run() {
		for(int i = 1; i <= 5; i++) {
			makeWithdrawal(2000);
			if(account.getBalance() < 0) {
				System.out.println("Account is overdrawn");
			}
		}
	}
	
	private synchronized void makeWithdrawal(int wamount) {
		if(account.getBalance() >= wamount) {
			System.out.println(Thread.currentThread().getName() + " is going to withdraw " + wamount);
			
			account.withdraw(wamount);
			System.out.println(Thread.currentThread().getName() + " has withdrawn " + wamount);
		}
		else {
			System.out.println("Not enough amount to withdraw " + Thread.currentThread().getName() + " : " + account.getBalance());
		}
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
