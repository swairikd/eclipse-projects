import java.util.*;

public class AreaOfRectangle {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		System.out.print("enter length : ");
		float length = in.nextFloat();
		System.out.print("enter breadth : ");
		float breadth = in.nextFloat();
		
		System.out.format("The area is : %.3f", length * breadth);
		
		in.close();

	}

}
