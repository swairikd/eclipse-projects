package recap;

import java.util.*;

import java.util.Map.*;
import java.util.stream.*;

/*
1.  print employee details based on department
2.  print employees count working in each department
3.  print active and inactive employees in the given list
4.  print max/min salary of employee from the given list
5.  print max salary of employees from the given list 
*/

public class EmployeeStream {

	public static void main(String[] args) {
		ArrayList<Employee> list = new ArrayList<>();
		list.add(new Employee(101, "Sai", "active", 555, 4400));
		list.add(new Employee(109, "Rama", "active", 555, 5200));
		list.add(new Employee(107, "John", "active", 222, 2700));
		list.add(new Employee(108, "Shree", "active", 555, 6400));
		list.add(new Employee(106, "Reddy", "inactive", 333, 3400));
		list.add(new Employee(105, "Tanya", "active", 555, 2400));
		list.add(new Employee(104, "Punya", "active", 222, 200));
		list.add(new Employee(103, "Doe", "active", 555, 2600));
		
//		Qsn1
		int d = 555;
		List<Employee> employeListDept = list.stream()
										.filter(e -> e.getDeptid() == d)
										.collect(Collectors.toList());
		employeListDept.forEach(System.out::println);
		
//		Qsn2
		Map<Integer, Integer> mp = new HashMap<>();
		list.stream().forEach(e -> mp.put(
										e.getDeptid(),
										mp.getOrDefault(e.getDeptid(), 0) + 1)
										);
		
		for(Entry<Integer, Integer> entry : mp.entrySet()) {
			System.out.println("Dept " + entry.getKey() + " : " + entry.getValue());
		}
		
//		Qsn3
		List<Employee> activeList = new ArrayList<>();
		List<Employee> inActiveList = new ArrayList<>();
		
//		list.stream().forEach(e -> {e.getStatus().equals("active") ? activeList.add(e) : inActiveList.add(e); return;});
		
		list.stream().forEach(e -> {
								if (e.getStatus().equalsIgnoreCase("active")) 
									activeList.add(e); 
								else 
									inActiveList.add(e);
								}); 
		System.out.println("Active Employees : ");
		activeList.stream().forEach(System.out::println);
		
		System.out.println("Inactive Employees : ");
		inActiveList.stream().forEach(System.out::println);
		
//		Qsn4
		Employee mx = list.stream().max(Employee::compareTo).get();
		System.out.println("max : " + mx);
		
		Employee mn = list.stream().min(Employee::compareTo).get();
		System.out.println("min : " + mn);

//		Qsn5
		List<Employee> maxEmployees = new ArrayList<>();
		list.stream().forEach(e -> {
								if(e.getSalary() == mx.getSalary())	maxEmployees.add(e);
								}
				);
		System.out.println("List of max employees : ");
		System.out.println(maxEmployees);
		
	}

}
