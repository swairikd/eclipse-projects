package recap;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.Supplier;

public class LambdaMethodRefDemo {

	public static void main(String[] args) {
		IntPredicate intPredicate = (x) -> x % 2 == 1;
		
//		BiFunction<Integer, Integer, Integer> biFunction = (a, b) -> a > b ? a : b;
		BiFunction<Integer, Integer, Integer> biFunction = Integer::max;
		
		System.out.println(intPredicate.test(11));
		
		System.out.println(biFunction.apply(2, 5));
		
		Function<Integer, String> f1 = (x) -> Integer.toBinaryString(x);
		System.out.println(f1.apply(10));
		
		Function<Integer, String> f2 = Integer::toBinaryString;
		System.out.println(f2.apply(10));
		
		MyRunnable myRunnable = (x, y) -> x + y;
		System.out.println(myRunnable.test(2, 6));
		
		Addition adder = new Addition();
		
		MyRunnable myRunnable2 = adder::add;
		System.out.println(myRunnable2.test(3, 6));
		
		Supplier<String> supplier1 = () -> new String();
		System.out.println("Empty String : " + supplier1.get());
		
		Supplier<String> supplier2 = String::new;
		System.out.println("Empty String : " + supplier2.get());
		
	}

}
