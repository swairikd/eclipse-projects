package recap;

public class Employee implements Comparable<Employee> {
	private int empid;
	private String name;
	private String status;
	private int deptid;
	private int salary;
	
	public Employee(int empid, String name, String status, int deptid, int salary) {
		super();
		this.empid = empid;
		this.name = name;
		this.status = status;
		this.deptid = deptid;
		this.salary = salary;
	}

	public int getEmpid() {
		return empid;
	}

	public void setEmpid(int empid) {
		this.empid = empid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getDeptid() {
		return deptid;
	}

	public void setDeptid(int deptid) {
		this.deptid = deptid;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", name=" + name + ", status=" + status + ", deptid=" + deptid + ", salary="
				+ salary + "]";
	}

	@Override
	public int compareTo(Employee o) {
		return this.getSalary() - o.getSalary();
	}
	
}
