package recap;

public abstract class MyAbstract {
	
	int num;
	String str;
	
	MyAbstract(int num, String str) {
		this.num = num;
		this.str = str;
	}
	
	void foo() {
		System.out.println("foo from MyAbstract class");
	}
	
	abstract void bar();
	
}
