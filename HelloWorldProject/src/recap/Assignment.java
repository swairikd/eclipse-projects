package recap;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collectors;

/*
Exercise 1: Write a lambda expression which accepts x and y numbers and return x to the power of y.

Exercise 2: Write a method that uses lambda expression to format a given string, where a space is inserted between each character of string.  For ex., if input is “COMPANY”, then expected output is “C O M P A N Y”.

Exercise 3: Write a method that uses lambda expression to accept username and password and return true or false. (Hint: Use any custom values for username and password for authentication)

Exercise 4: Write a class with main method to demonstrate instance creation using method reference. (Hint: Create any simple class with attributes and getters and setters)

Exercise 5: Write a method to calculate factorial of a number. Test this method using method reference feature.
  
*/

class Person {
	private String name;
	private int age;
	
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
}

public class Assignment {

	public static void main(String[] args) {
		BiFunction<Integer, Integer, Integer> pow = (x, y) -> {
			int ans = 1;
			while(y > 0) {
				if(y % 2 == 1) {
					ans *= x;
				}
				x *= x;
				y >>= 1;
			}
			return ans;
		};
		
		System.out.println(pow.apply(3, 5));
		
		String str = "COMPANY";
		Function<String, String> fn = (s) -> s.replace("", " ").trim();
		System.out.println(fn.apply(str));
		
		BiFunction<String, String, Boolean> check = (u, p) -> u == "me" && p == "pass";
		System.out.println(check.apply("me", "pass"));
		
		
		
	}

}
