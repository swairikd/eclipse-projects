package recap;

public class Addition {
	int add(int x, int y) {
		return x + y;
	}
	
	int add(long x, long y) {
		return (int) (x + y);
	}
	
	int add(byte x, byte y) {
		return x + y;
	}
	
	int add(short x, short y) {
		return x + y;
	}
	
}
