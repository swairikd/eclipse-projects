package recap;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamDemo {

	public static void main(String[] args) {
		int[] integers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		
//		2,4,6,8
//		4 + 16 + 36 + 64
//		120
		
//		find even numbers, then square them, then get their sum
		
//		int ans = Arrays.stream(integers)
//							.filter(n -> n % 2 == 0)
//							.map(n -> n * n)
//							.sum();
		
		int ans = IntStream.of(integers)
							.filter(n -> n % 2 == 0)
							.map(n -> n * n)
							.sum();
		
		System.out.println(ans);
		
//		Stream<Integer> integers = Stream.of(1, 2, 3, 4, 5);
//		integers.forEach(System.out::println);
		
	}

}
