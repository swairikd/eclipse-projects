package day9;

@FunctionalInterface
interface AddInter {
	int add(int value);
}

public class FuncInter {

	public static void main(String[] args) {
//		implementing FI using lambda expressions
		AddInter ai = (int val) -> val + 100;
		System.out.println("Result is : " + ai.add(100));
		
	}

}
