package day9;

import java.util.*;

public class MethodRefInstance {

	public static void main(String[] args) {
		List<String> names = new ArrayList<>();
		names.add("Peter");
		names.add("Bruce");
		names.add("Clark");
		
		Collections.sort(names, String::compareToIgnoreCase);
		names.forEach(System.out::println);
	}

}
