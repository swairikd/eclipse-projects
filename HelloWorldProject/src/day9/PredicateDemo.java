package day9;

import java.util.function.Predicate;

public class PredicateDemo {

	public static void main(String[] args) {
		Predicate<Integer> predicate = (a) -> a % 2 == 0;
		if(predicate.test(68)) {
			System.out.println("Even Number");
		}
		else {
			System.out.println("Odd number");
		}
		
		Predicate<String> predicate1 = Predicate.isEqual("Java");
		System.out.println(predicate1.test("Java"));
		
//		and/or
		Predicate<Integer> marks = (x) -> x > 0;
		Predicate<Integer> marks2 = (x) -> x < 100;
		System.out.println(marks.and(marks2).test(161));
		System.out.println(marks.or(marks2).test(61));
		
	}

}
