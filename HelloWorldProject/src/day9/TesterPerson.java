package day9;

import java.util.*;

public class TesterPerson {

	public static int compareByName(Person p1, Person p2) {
		return p1.getName().compareTo(p2.getName());
	}
	
	public static int compareByAge(Person p1, Person p2) {
        return p1.getAge().compareTo(p2.getAge());
    }
	
	public int nonStaticCompareByName(Person p1, Person p2) {
		return p1.getName().compareTo(p2.getName());
	}
	
	public int nonStaticCompareByAge(Person p1, Person p2) {
        return p1.getAge().compareTo(p2.getAge());
    }
	
	public static void main(String[] args) {
		List<Person> pList = new ArrayList<>();
		pList.add(new Person("Mary", 27));
		pList.add(new Person("Harry", 26));
		pList.add(new Person("Doc", 28));
		pList.add(new Person("Peter", 25));
		
//		Collections.sort(pList, TesterPerson::compareByName);
//		System.out.println("Sorted By Name ");
//		pList.stream()
//				.map(x -> x.getName())
//				.forEach(System.out::println);
//		
//		Collections.sort(pList, TesterPerson::compareByAge);
//		System.out.println("Sorted by Age");
//		pList.stream()
//				.map(x -> x.getAge())
//				.forEach(System.out::println);
		
		
//		Non-static method references
		TesterPerson obj = new TesterPerson();
		System.out.println("Sorted By Name ");
		Collections.sort(pList, obj::nonStaticCompareByName);
		pList.stream()
				.map(x -> x.getName())
				.forEach(System.out::println);

		Collections.sort(pList, obj::nonStaticCompareByAge);
		System.out.println("Sorted by Age");
		pList.stream()
				.map(x -> x.getAge())
				.forEach(System.out::println);
	}

}
