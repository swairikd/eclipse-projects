package day9;

public class Student {
	private String name;

	public Student(String name) {
		this.name = name;
	}
	
//	public void qwe () {
//		System.out.println("qyue");
//	};
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + "]";
	}
	
}
