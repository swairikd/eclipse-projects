package day9;

import java.util.Optional;

public class OptionalDemo {

	public static void main(String[] args) {
		String[] str = new String[20];
//		str[0] = "Optional Demo";
		
//		apply some function to str
		
		/*
		Optional<String> checkNull = Optional.ofNullable(str[0]);
		checkNull.ifPresent(System.out::println);
		
		if(checkNull.isPresent()) {
			String str2 = str[0].toUpperCase();
			System.out.println(str2);
		}
		else {
			System.out.println("Value is not present");
		}
		*/
		
//		empty method
		Optional<String> empty = Optional.empty();
		System.out.println(empty);
		
		if(empty.isPresent())
			System.out.println(empty.get());
		else
			System.out.println("no val");
		
//		of method
//		Optional<String> value = Optional.of(str[0]);
//		
////		get method
//		System.out.println("Printing value : " + value.get());
//		
////		hashcode
//		System.out.println("printing hashCode : " + value.hashCode());
//		
//		System.out.println("Printing hashcode : " + value.orElse("Value is not there"));
	}

}
