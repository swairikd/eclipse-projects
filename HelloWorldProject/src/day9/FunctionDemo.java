package day9;

import java.util.*;
import java.util.function.Function;

public class FunctionDemo {

	public static void main(String[] args) {
		List<Integer> numbers = new ArrayList<>();
		
		numbers.add(678);
		numbers.add(456);
		numbers.add(345);
		
		Function<Integer, Float> funobj = (n) -> (float)n / 2;
		
		funobj = funobj.andThen(n -> n * 5);
		
//		numbers.stream().map(funobj).forEach(System.out::println);
		
		System.out.println(funobj.apply(77));
		
	}

}
