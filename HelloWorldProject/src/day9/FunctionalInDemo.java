package day9;

@FunctionalInterface
interface demo {
	void print(String msg);
//	void print1(String msg);
}

public class FunctionalInDemo implements demo {
	
	@Override
	public void print(String msg) {
		System.out.println(msg);
	}
	
	public static void main(String[] args) {
		FunctionalInDemo obj1 = new FunctionalInDemo();
		obj1.print("Functional Interface Demo");
	}

}
