package day9;

@FunctionalInterface
interface MyInterface {
	Student getStudent(String name);
}

public class MethodRefCons {

	public static void main(String[] args) {
		MyInterface obj = Student :: new;
		
		System.out.println(obj.getStudent("Peter"));
		
		
//		Student sobj = new Student("Bruce");
//		Stu s = sobj::qwe;
//		s.abc(;
	}

}
