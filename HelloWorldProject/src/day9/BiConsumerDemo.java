package day9;

import java.util.function.BiConsumer;

public class BiConsumerDemo {

	public static void main(String[] args) {
		BiConsumer<String, String> biconsumer = (a, b) -> System.out.println(a + b);
		biconsumer.accept("Hello", " World");
		
		BiConsumer<Integer, Integer> biconsumer2 = (a, b) -> System.out.println(a + b);
		biconsumer2.accept(56, 89);
	}
}
