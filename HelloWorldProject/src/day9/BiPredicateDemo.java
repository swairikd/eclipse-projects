package day9;

import java.util.function.BiPredicate;

public class BiPredicateDemo {

	public static void main(String[] args) {
		BiPredicate<String, String> bpobj = (s1, s2) -> s1.startsWith(s2);
		BiPredicate<String, String> bpobj2 = (s1, s2) -> s1.endsWith(s2);
		
//		s1 should start with s2 and end with s2
		System.out.println(bpobj.and(bpobj2).test("aba", "a"));
		System.out.println(bpobj.and(bpobj2).test("abc", "a"));
		
		BiPredicate<Integer, Integer> bpobj3 = (n1, n2) -> (n1 % n2 == 0);
		System.out.println(bpobj3.test(55, 11));
		System.out.println(bpobj3.test(55, 10));
		
		System.out.println(bpobj3.negate().test(55, 10));
	}
}
