package stock;

import java.util.UUID;

public class Inventory {
	private int quantity;
	private final int lowOrderLevelQuantity;
	private UUID uniqueId;
	
	public Inventory(int quantity, int lowOrderLevelQuantity) {
		this.uniqueId = UUID.randomUUID();
		this.quantity = quantity;
		this.lowOrderLevelQuantity = lowOrderLevelQuantity;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public UUID getUniqueId() {
		return uniqueId;
	}

	public int getLowOrderLevelQuantity() {
		return lowOrderLevelQuantity;
	}
	
}
