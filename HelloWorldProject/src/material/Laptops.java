package material;

import stock.Inventory;

public class Laptops extends Inventory {
	private String company;

	public Laptops(int quantity, int lowOrderLevelQuantity, String company) {
		super(quantity, 3);
		this.company = company;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	
}
