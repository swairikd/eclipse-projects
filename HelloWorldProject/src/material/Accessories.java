package material;

import stock.Inventory;

public class Accessories extends Inventory {
	private String type;

	public Accessories(int quantity, String type) {
		super(quantity, 5);
		this.setType(type);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
