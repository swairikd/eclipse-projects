package day4;

import java.util.ArrayList;

public class ArrayListDemo {
	public static void main(String[] args) {
		ArrayList<String> langs = new ArrayList<>();
		langs.add("Java");
		langs.add("Python");
		langs.add("CPP");
		
		System.out.println("ArrayList is : " + langs);
		System.out.println("Element at index 1 : " + langs.get(1));
		langs.set(2, "Kotlin");
		System.out.println("ArrayList is : " + langs);
		
		String str = langs.remove(2);
		System.out.println("ArrayList is : " + langs);
		
//		size() => returns size
//		sort() => sorts elements
//		clone() => creates a new arraylist is created
		
		for(String st : langs) {
			System.out.println(st);
		}
		
	}
}
