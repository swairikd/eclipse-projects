package day4;

public class Employee {
	private int empId;
	private String empName;
	
	public Employee(int empId, String empName) {
		super();
		this.empId = empId;
		this.empName = empName;
		System.out.println("Employee Constructor Called");
	}
	
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	
	public void display() {
		System.out.println("Employee Details:");
		System.out.println("Employee Id : " + this.empId);
		System.out.println("Employee Name : " + this.empName);
	}
}
