package day4;

public class Tester {

	public static void main(String[] args) {
		Manager obj = new Manager(101, "Bruce", "Assistant Manager");
		obj.display();
		System.out.println("Designation : " + obj.getDesignation());
	}

}
