package day4;

public class TesterUser {
	public static void main(String[] args) {
		User user1 =  new User(101, "Peter", "peter@gmail.com", 100);
		PremiumUser user2 = new PremiumUser(202, "Bruce", "bruce@gmail.com", 300, 0);
		
		processPayment(user1, 70);
		processPayment(user2, 50);
		
	}
	
	public static void processPayment(User user, double billAmount) {
		if(user.makePayment(billAmount)) {
			System.out.println("Payment of bill amount : " + billAmount + " has been done successfully");
		}
		else {
			System.out.println("Payment of bill amount : " + billAmount + " has not been done due to insufficient balance");
		}
		System.out.println("Balance left is : " + user.getBalance());
		if(user instanceof PremiumUser) {
			PremiumUser pUser = (PremiumUser)user;
			System.out.println("You have " + pUser.getRewardPoints() + " reward points");
		}
	}
}
