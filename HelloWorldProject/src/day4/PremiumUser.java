package day4;

public class PremiumUser extends User {
	int rewardPoints = 0;

	public PremiumUser(int id, String userName, String email, double balance, int rewardPoints) {
		super(id, userName, email, balance);
		this.rewardPoints = rewardPoints;
	}

	public int getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(int rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	@Override
	public boolean makePayment(double billAmount) {
		if(balance > billAmount) {
			balance -= billAmount;
			rewardPoints += (billAmount * 10) / 100;
			return true;
		}
		else {
			return false;
		}
	}
	
}
