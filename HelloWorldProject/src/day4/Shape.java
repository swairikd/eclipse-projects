package day4;

public class Shape {
	public double calArea(float rad) {
		return 3.14 * rad * rad;
	}
	 
	public double calArea(float length, float breadth) {
		return length * breadth;
	}
}
