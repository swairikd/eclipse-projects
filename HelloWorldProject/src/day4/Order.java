package day4;

public class Order {
	int orderId;
	String orderedFoods;
	double totalPrice;
	String status;
	
	public Order() {
		System.out.println("Default called");
	}

	public Order(int orderId, String orderedFoods) {
		this.orderId = orderId;
		this.orderedFoods = orderedFoods;
		System.out.println("2 parameter constructor called");
	}
	
	
	public Order(int orderId, String orderedFoods, double totalPrice, String status) {
		this.orderId = orderId;
		this.orderedFoods = orderedFoods;
		this.totalPrice = totalPrice;
		this.status = status;
		System.out.println("All parameter constructor called");
	}

	public Order(int orderId, String orderedFoods, String status) {
		this.orderId = orderId;
		this.orderedFoods = orderedFoods;
		this.status = status;
		System.out.println("3 parameter constructor called");
	}

	//	calculate()  : returns total price
	double calculateTotalPrice(int unitPrice) {
		totalPrice = unitPrice + (5 * unitPrice);
		return totalPrice;
	}	

}
