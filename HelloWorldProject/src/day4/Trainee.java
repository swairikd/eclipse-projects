package day4;

public class Trainee {
	static int counter = 1;
	private String regId;
	private String name;
	private long contactNumber;
	private String city;
	
	public Trainee(String name, long contactNumber, String city) {
		super();
		this.name = name;
		this.contactNumber = contactNumber;
		this.city = city;
	}

	public static int getCounter() {
		return counter;
	}

	public static void setCounter(int counter) {
		Trainee.counter = counter;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
	
}
