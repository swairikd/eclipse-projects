package day4;

public class TesterOrder {

	public static void main(String[] args) {
		Order o1 = new Order();
		Order o2 = new Order(201, "Pizza");
		Order o3 = new Order(301, "Fries", 0.0, "Ordered");
		Order o4 = new Order(301, "Fries", "Ordered");
		
//		o1.orderId = 101;
//		o1.orderedFoods = "Pasta";
//		o1.status = "Ordered";
		System.out.println("Order1 status : " + o1.status);
		System.out.println("Order1 Food : " + o1.orderedFoods);
		System.out.println("Order2 status : " + o2.status);
		System.out.println("Order2 Foods : " + o2.orderedFoods);
//		o1.totalPrice = o1.calculateTotalPrice(10);
		
//		System.out.println("Order Details : ");
//		System.out.println("Total Price : " + o1.totalPrice);
	}

}
