package day4;

public class TesterShape {
	public static void main(String[] args) {
		Shape s1 = new Shape();
		double c_Area = s1.calArea(5.75F);
		double r_Area = s1.calArea(3.5F, 5.7F);
		
		System.out.println("Circle Area is " + c_Area);
		System.out.println("Rectangle Area is " + r_Area);
	}
}
