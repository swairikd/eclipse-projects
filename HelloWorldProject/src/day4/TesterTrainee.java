package day4;

public class TesterTrainee {

	public static void main(String[] args) {
		Trainee t1 = new Trainee("Bruce", 123456789L, "Gotham");
		Trainee t2 = new Trainee("Peter", 7894561223L, "Queens");
		Trainee t3 = new Trainee("Clark", 456789123L, "Metropolis");
		
		Trainee t[] = {t1, t2, t3};
		
		for(Trainee i : t) {
			System.out.println("Name : " + i.getName());
			System.out.println("Contact No. : " + i.getContactNumber());
		}
		
	}

}
