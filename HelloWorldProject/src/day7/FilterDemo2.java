package day7;

import java.util.*;
import java.util.stream.Collectors;

class Product {
	int id;
	String name;
	double price;
	
	public Product(int id, String name, double price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
}

public class FilterDemo2 {
	public static void main(String[] args) {
		List<Product> productList = new ArrayList<>();
		productList.add(new Product(101, "Laptop", 40000));
		productList.add(new Product(102, "Camera", 25000));
		productList.add(new Product(103, "Smartphone", 50000));
		productList.add(new Product(104, "Smart TV", 35000));
		productList.add(new Product(105, "Mouse", 4000));
		
//		filter based on price greater than 30000
		List<Product> products = productList.stream().filter(p -> p.price > 30000).collect(Collectors.toList()); 
		
		System.out.println(products);
		
	}
}
