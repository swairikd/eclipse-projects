package day7;

import java.util.*;
import java.util.stream.Collectors;

public class MapDemo {

	public static void main(String[] args) {
		List<String> namesList = Arrays.asList("Bruce", "Peter", "Lewis", "Daniel");
		
		List<String> upperNamesList = new ArrayList<>();
		upperNamesList = namesList.stream().map(name -> name.toUpperCase()).collect(Collectors.toList());
		
		System.out.println(upperNamesList);
		
	}

}
