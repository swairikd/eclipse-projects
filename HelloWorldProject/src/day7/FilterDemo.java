package day7;

import java.util.*;
import java.util.stream.Collectors;

public class FilterDemo {

	public static void main(String[] args) {
		List<Integer> numberList = Arrays.asList(29, 30, 45, 50, 60, 79);
		List<Integer> evenNumberList = new ArrayList<Integer>();
		
//		without streams
		for(int n : numberList) {
			if(n % 2 == 0)
				evenNumberList.add(n);
		}
		System.out.println(evenNumberList);
		
//		with streams
		evenNumberList = numberList.stream().filter(n -> n % 2 == 0).collect(Collectors.toList());
		System.out.println(evenNumberList);
		
		numberList.stream().filter(n -> n % 2 == 0).forEach(n -> System.out.println(n));
		
	}

}
