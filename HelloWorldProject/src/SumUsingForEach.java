import java.util.Arrays;

public class SumUsingForEach {

	public static void main(String[] args) {
		int []a = {1, 2, 3, 4};
		int sum = 0;
		for(int n : a) {
			sum += n;
		}
		System.out.format("The sum is %d\n", sum);
	}

}
