package day5;

public abstract class Student {
	private String name;
	private int[] testScores;
	private String result;
	private static final int totalTests = 4;
	
	public Student(String name) {
		super();
		this.name = name;
		this.testScores = new int[totalTests];
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int[] getTestScores() {
		return testScores;
	}

	public void setTestScore(int testNumber, int testScore) {
		if(testNumber < 0 || testNumber >= totalTests) {
			System.out.println("Wrong test number!!!");
			return;
		}
		this.testScores[testNumber] = testScore;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	public abstract void generateResult();
}
