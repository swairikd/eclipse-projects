package day5;

import java.util.Arrays;

public class UGStudent extends Student {
	public UGStudent(String name) {
		super(name);
	}

	@Override
	public void generateResult() {
		int res = Arrays.stream(getTestScores()).sum() / getTestScores().length;
		
		if(res >= 60) {
			setResult("Pass");
		}
		else {
			setResult("Fail");
		}
	}
	
}
