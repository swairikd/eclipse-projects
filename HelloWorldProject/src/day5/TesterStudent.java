package day5;

public class TesterStudent {
	public static void main(String[] args) {
		Student s1 = new UGStudent("Bruce");
		s1.setTestScore(0, 61);
		s1.setTestScore(1, 61);
		s1.setTestScore(2, 61);
		s1.setTestScore(3, 61);
		s1.generateResult();
		
		Student s2 = new UGStudent("Peter");
		s2.setTestScore(0, 59);
		s2.setTestScore(1, 59);
		s2.setTestScore(2, 59);
		s2.setTestScore(3, 59);
		s2.generateResult();
		
		Student s3 = new PGStudent("Seb");
		s3.setTestScore(0, 71);
		s3.setTestScore(1, 71);
		s3.setTestScore(2, 71);
		s3.setTestScore(3, 71);
		s3.generateResult();
		
		Student s4 = new PGStudent("Daniel");
		s4.setTestScore(0, 59);
		s4.setTestScore(1, 59);
		s4.setTestScore(2, 59);
		s4.setTestScore(3, 59);
		s4.generateResult();
		
		System.out.println("Student Name : " + s1.getName() + " Result : " + s1.getResult());
		
		System.out.println("Student Name : " + s2.getName() + " Result : " + s2.getResult());

		System.out.println("Student Name : " + s3.getName() + " Result : " + s3.getResult());

		System.out.println("Student Name : " + s4.getName() + " Result : " + s4.getResult());

	}
}
