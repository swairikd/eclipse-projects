package day5;

class NegativeMarksException extends Exception{
	public NegativeMarksException(String message) {
		super(message);
	}
}

public class StudentEx {
	private String name;
	private int[] marks;
	private float avgMarks;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int[] getMarks() {
		return marks;
	}
	public void setMarks(int[] marks) {
		this.marks = marks;
	}
	public float getAvgMarks() {
		return avgMarks;
	}
	public void setAvgMarks(float avgMarks) {
		this.avgMarks = avgMarks;
	}
	
	public void calcAvgMarks() {
		int sum = 0;
		int totalSub = 0;
		try {
			try {
				for(int i = 0; i < marks.length; i++) {
					if(marks[i] < 0) {
						throw new NegativeMarksException("Marks cannot be negative");
					}
					totalSub++;
					sum += marks[i];
				}
				this.avgMarks = sum / totalSub;
				System.out.println("Average Marks : " + this.avgMarks);
			}
			catch(ArithmeticException e) {
				System.out.println("Division by 0 exception");
			}
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Array Index Bounds exception");
		}
		catch(Exception e) {
			System.out.println(e.getMessage());
		}
		finally {
			System.out.println("Thank you for using our application");
		}
	}
	
}
