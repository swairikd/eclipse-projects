package day5;

public interface Tax {
	public double calculateTax(double amount);
}
