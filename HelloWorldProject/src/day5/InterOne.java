package day5;

public interface InterOne {
	void display();
	
	default void welcomeMsg() {
		System.out.println("Welcome");
	}
	
	static int cube(int x) {
		return x * x * x;
	}
}
