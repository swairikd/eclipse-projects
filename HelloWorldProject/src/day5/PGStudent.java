package day5;

import java.util.Arrays;

public class PGStudent extends Student {

	public PGStudent(String name) {
		super(name);
	}

	@Override
	public void generateResult() {
		int res = Arrays.stream(getTestScores()).sum() / getTestScores().length;
		
		if(res >= 70) {
			setResult("Pass");
		}
		else {
			setResult("Fail");
		}
	
	}
	
}
