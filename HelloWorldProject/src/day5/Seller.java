package day5;

public class Seller implements Tax {
	private String location;
	private double taxPercent;
	
	public Seller(String location) {
		super();
		this.location = location;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(double taxPercent) {
		this.taxPercent = taxPercent;
	}

	@Override
	public double calculateTax(double price) {
		if(location == "India") 
			this.taxPercent = 10;
		else if(location == "Europe")
			this.taxPercent = 20;
		else if(location == "US")
			this.taxPercent = 15;
		else if(location == "Canada")
			this.taxPercent = 25;
		
		return (price + (price * this.taxPercent) / 100);
	}
	
}
