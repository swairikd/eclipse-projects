package day5;

public class PurchaseDetails implements Tax {
	private String purchaseId;
	private String paymentType;
	private double taxPercent;
	
	public PurchaseDetails(String purchaseId, String paymentType) {
		super();
		this.purchaseId = purchaseId;
		this.paymentType = paymentType;
	}

	public String getPurchaseId() {
		return purchaseId;
	}

	public void setPurchaseId(String purchaseId) {
		this.purchaseId = purchaseId;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public double getTaxPercent() {
		return taxPercent;
	}

	public void setTaxPercent(double taxPercent) {
		this.taxPercent = taxPercent;
	}
	
	@Override
	public double calculateTax(double price) {
		if(paymentType == "Credit Card") 
			this.taxPercent = 1;
		else if(paymentType == "Debit Card")
			this.taxPercent = 2;
		else
			this.taxPercent = 4;
		return (price + (price * this.taxPercent) / 100);
	}
	

}
