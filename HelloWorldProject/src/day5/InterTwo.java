package day5;

public interface InterTwo extends InterOne {
	void show();
	default void welcomeMsg() {
		System.out.println("Welcome");
	}
}
