package day5;

public class TesterTax {

	public static void main(String[] args) {
		PurchaseDetails p = new PurchaseDetails("101", "Credit Card");
		System.out.println(p.calculateTax(562.5));
		
		Seller s = new Seller("India");
		System.out.println(s.calculateTax(1234.78));
	}

}
