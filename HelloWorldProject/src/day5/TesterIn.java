package day5;

public class TesterIn implements InterOne, InterTwo {

	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("hello");
	}
	
	@Override
	public void welcomeMsg() {
		System.out.println("Welcome from class");
	}
	
	@Override
	public void show() {
		System.out.println("hello from show method");
	}
	
	public static void main(String[] args) {
		TesterIn obj = new TesterIn();
		obj.display();
		obj.show();
		obj.welcomeMsg();
		System.out.println(InterOne.cube(4));
	}
	
}
