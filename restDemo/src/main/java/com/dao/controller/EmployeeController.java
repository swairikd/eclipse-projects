package com.dao.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dao.model.EmployeeModel;

@RestController
public class EmployeeController {

	@RequestMapping(value = "/getAllEmployees", method = RequestMethod.GET)
	public List<EmployeeModel> getAllEmployees() {
		return employees();
	}

	@RequestMapping(value = "/getEmployeeById/{id}", method = RequestMethod.GET)
	public EmployeeModel getEmployeeById(@PathVariable int id) {
		List<EmployeeModel> emps = employees();
		for (EmployeeModel emp : emps) {
			if (emp.getEmployeeId() == id) {
				return emp;
			}
		}
		return null;
	}

	public List<EmployeeModel> employees() {
		EmployeeModel emp1 = new EmployeeModel(1001, "Bruce", "Gotham", "CEO");
		EmployeeModel emp2 = new EmployeeModel(1002, "Peter", "Queens", "Photographer");
		EmployeeModel emp3 = new EmployeeModel(1003, "Clark", "Metropolis", "Reporter");
		EmployeeModel emp4 = new EmployeeModel(1004, "Tony", "New York", "CEO");
		EmployeeModel emp5 = new EmployeeModel(1005, "Barry", "City", "CSI");

		List<EmployeeModel> emps = new ArrayList<>();
		emps.add(emp1);
		emps.add(emp2);
		emps.add(emp3);
		emps.add(emp4);
		emps.add(emp5);

		return emps;
	}

}
