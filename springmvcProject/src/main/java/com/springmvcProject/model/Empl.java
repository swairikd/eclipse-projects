package com.springmvcProject.model;

import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class Empl {
	private int id;

	@Size(min = 3, message = "min username size should be 3")
	private String name;

	@Positive(message = " salary should be >=0")
	private int salary;

	private String designation;

	public Empl() {
		super();
	}

	public Empl(int id, String name, int salary, String designation) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.designation = designation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	@Override
	public String toString() {
		return "Empl [id=" + id + ", name=" + name + ", salary=" + salary + ", designation=" + designation + "]";
	}

}
