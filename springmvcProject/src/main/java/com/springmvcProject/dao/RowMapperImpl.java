package com.springmvcProject.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springmvcProject.model.Empl;

public class RowMapperImpl implements RowMapper<Empl> {

	public Empl mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empl e = new Empl();
		e.setId(rs.getInt(1));
		e.setName(rs.getString(2));
		e.setSalary(rs.getInt(3));
		e.setDesignation(rs.getString(4));

		return e;
	}

}
