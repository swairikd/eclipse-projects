package com.springmvcProject.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.springmvcProject.model.Empl;

public class EmplDao {

	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	public int save(Empl e) {
		String query = "insert into empl(id,name,salary,designation) values(?,?,?,?)";

		int r = this.jdbcTemplate.update(query, e.getId(), e.getName(), e.getSalary(), e.getDesignation());

		return r;
	}

	public int update(Empl e) {
		// update
		String query = "update empl set name=?, salary=?, designation=? where id=?";

		int r = this.jdbcTemplate.update(query, e.getName(), e.getSalary(), e.getDesignation(), e.getId());

		return r;
	}

	public int delete(int id) {
//		update
		String query = "delete from empl where id=?";

		int r = this.jdbcTemplate.update(query, id);

		return r;
	}

	public Empl getEmpl(int id) {
//		queryForObject -> rowmapper
		String query = "select * from empl where id=?";
		RowMapper<Empl> rowMapper = new RowMapperImpl();
		Empl e = this.jdbcTemplate.queryForObject(query, rowMapper, id);

		return e;

	}

	public List<Empl> getEmployees() {
		String query = "select * from empl";
		RowMapper<Empl> rowMapper = new RowMapperImpl();
		List<Empl> emps = this.jdbcTemplate.query(query, rowMapper);
//		List<Empl> emps = new ArrayList<Empl>();
//		emps.add(new Empl(2, "b", 87, "wer"));
		return emps;
	}


}
